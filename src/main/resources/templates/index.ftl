<!-- 防止jsp中文乱码 -->
<#--<%@page language="java" import="java.util.*" pageEncoding="UTF-8" %>-->
<html>
<head>
<#--    <%--引入css--%>-->
    <link href="${pageContext.request.contextPath}/static/layui/css/layui.css" rel='stylesheet'>
</head>
<body>
<div style="width: 70%;margin: 20px auto;">
    <h2>插入</h2>
    <form method="post" action="${pageContext.request.contextPath}/saveUser">
        <input name="id" value="0" id="id" style="display:none" />
        <table class="layui-table" lay-even lay-skin="row">
            <tr><td>用户名：</td><td><input class="layui-input" name="userName" id="userName"/></td></tr>
            <tr><td>密码：</td><td><input class="layui-input" name="password" id="password"/></td></tr>
            <tr style="text-align: center;">
                <td colspan="2">
                    <button class="layui-btn layui-btn-radius" type="submit">提交</button>
                    <button class="layui-btn layui-btn-radius" type="reset">重置</button>
                </td>
            </tr>
        </table>
    </form>
    <hr/>
    <h2>查询</h2>
    <table class="layui-table" lay-even lay-skin="row">
        <tr><td>用户名：</td><td><input class="layui-input" id="tag_userName"/></td>
            <td><button class="layui-btn layui-btn-radius" type="button" οnclick="listUserByUserName()">查询</button> </td></tr>
    </table>
    <hr/>
    <h2>列表</h2>
    <table cellspacing="30px" class="layui-table" lay-even lay-skin="row">
        <thead><tr><td>id</td><td>用户名</td><td>密码</td><td>操作</td></tr></thead>
        <tbody id="list">

        </tbody>
    </table>
</div>

<#--<%--引入js--%>-->
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.min.js"></script>
<script>
    $(listUsers());

    //列出所有用户
    function listUsers() {
        $.post("${pageContext.request.contextPath}/listUsers",function (msg) {
            var data = msg.data;
            var content = '';
            for(var i = 0;i<data.length;i++){
                content+= "<tr><td>"+data[i].id+"</td><td>"+data[i].userName+"</td><td>"+data[i].password+"</td>"
                    +"<td><a href='javascript:void(0)' οnclick=updateUser('"+data[i].id+"','"+data[i].userName+"','"+data[i].password+"')>编辑</a>"
                    +"<a style='margin-left:10px;' href='${pageContext.request.contextPath}/removeUserById?id="+data[i].id+"'>删除</a></td></tr>";
            }
            $("#list").append(content);
        });
    };

    //编辑用户
    function updateUser(id,userName,password){
        $("#id").val(id);
        $("#userName").val(userName);
        $("#password").val(password);
    }

    //查询用户
    function listUserByUserName(){
        $.post("${pageContext.request.contextPath}/listUserByUserName",{userName:$("#tag_userName").val()},function (msg) {
            var data = msg.data;
            var content = '';
            $("#list").html('');
            for(var i = 0;i<data.length;i++){
                content+= "<tr><td>"+data[i].id+"</td><td>"+data[i].userName+"</td><td>"+data[i].password+"</td>"
                    +"<td><a href='javascript:void(0)' οnclick=updateUser('"+data[i].id+"','"+data[i].userName+"','"+data[i].password+"')>编辑</a></td>"
                    +"<td><a href='${pageContext.request.contextPath}/removeUserById?id="+data[i].id+"'>删除</a></td></tr>";
            }
            $("#list").append(content);
        });
    }

</script>
</body>
</html>