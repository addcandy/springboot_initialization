package com.shuo.initial.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @auther 烁烁
 * @date 2020/1/17 10:05
 */
@Controller
public class IndexControl {

    @RequestMapping("/")
    public String home(String name) {
//        return "redirect:/index";
        return "forward:/index";
    }
    @GetMapping("/index")
    public String getIndex(){
        return "index";
    }
}
