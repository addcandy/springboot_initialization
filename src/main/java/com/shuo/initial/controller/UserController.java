package com.shuo.initial.controller;

import com.shuo.initial.config.handlers.OneHandler;
import com.shuo.initial.model.domain.User;
import com.shuo.initial.model.other.ResultData;
import com.shuo.initial.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @auther 烁烁
 * @date 2020/4/27 11:15
 */

@Controller
@RequestMapping("/")
public class UserController {
    static Logger logger = LoggerFactory.getLogger(OneHandler.class);

    @Autowired
    private UserService userService;

    /**
     * 查 所有用户
     * @return
     */
    @RequestMapping("/listUsers")
    @ResponseBody
    public ResultData listUsers(){
        logger.info("日志打印:[0s0 哈哈哈！！]");
        return ResultData.success(userService.listUsers());
    }

    /**
     * 查 按条件查用户
     * @return
     */
    @RequestMapping("/listUserByUserName")
    @ResponseBody
    public ResultData listUserByUserName(String userName){
        return ResultData.success(userService.listUserByUserName(userName));
    }

    /**
     * 删 按id删除
     * @return
     */
    @RequestMapping("/removeUserById")
    public String removeUserById(@Param("id") int id){
        userService.removeUserById(id);
        return"index";
    }

    /**
     * 增 添加用户
     * @return
     */
    @RequestMapping("/saveUser")
    public String saveUser(User user){
        User exist = userService.getUserByIrd(user.getId());
        if(exist==null){
            userService.saveUser(user);
        }else{
            userService.updateUser(user);
        }
        return"index";
    }
}
