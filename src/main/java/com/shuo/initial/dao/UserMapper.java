package com.shuo.initial.dao;

import com.shuo.initial.model.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @auther 烁烁
 * @date 2020/4/27 11:28
 */
@Mapper
public interface UserMapper {

    /**
     * 列出所有用户
     * @return
     */
    List<User> listUsers();

    /**
     * 按照用户名查找
     * @param userName 用户名
     * @return
     */
    List<User> listUserByUserName(String userName);

    /**
     * 按照id查找用户
     * @param id 用户id
     * @return
     */
    User getUserById(int id);

    /**
     * 插入用户
     * @param user 待保存的用户
     * @return
     */
    int saveUser(@Param("user") User user);

    /**
     * 通过id删除用户
     * @param id
     * @return
     */
    int removeUserById(int id);

    /**
     * 更新用户
     * @param user
     * @return
     */
    int updateUser(@Param("user") User user);
}
