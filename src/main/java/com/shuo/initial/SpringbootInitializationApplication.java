package com.shuo.initial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootInitializationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootInitializationApplication.class, args);
    }

}
