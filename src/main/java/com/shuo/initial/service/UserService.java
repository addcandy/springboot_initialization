package com.shuo.initial.service;


import com.shuo.initial.model.domain.User;

import java.util.List;

/**
 * @auther 烁烁
 * @date 2020/4/27 11:30
 */

public interface UserService {

    List<User> listUsers();

    List<User> listUserByUserName(String userName);

    User getUserByIrd(int id);

    int saveUser(User user);

    int removeUserById(int id);

    int updateUser(User user);
}