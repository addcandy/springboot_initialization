package com.shuo.initial.service.impl;

import com.shuo.initial.dao.UserMapper;
import com.shuo.initial.model.domain.User;
import com.shuo.initial.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @auther 烁烁
 * @date 2020/4/27 11:30
 */

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Transactional
    public List<User> listUsers() {
        return userMapper.listUsers();
    }

    public List<User> listUserByUserName(String userName) {
        return userMapper.listUserByUserName(userName);
    }

    public User getUserByIrd(int id) {
        return userMapper.getUserById(id);
    }

    public int saveUser(User user) {
        return userMapper.saveUser(user);
    }

    public int removeUserById(int id) {
        return userMapper.removeUserById(id);
    }

    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }
}