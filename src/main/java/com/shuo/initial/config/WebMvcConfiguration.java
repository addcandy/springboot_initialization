package com.shuo.initial.config;

import com.shuo.initial.config.filters.OneFilter;
import com.shuo.initial.config.handlers.OneHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @auther 烁烁
 * @date 2020/5/11 12:35
 */
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer{

    @Autowired
    private OneFilter oneFilter;

    @Autowired
    private OneHandler oneHandler;

    @Bean
    public FilterRegistrationBean<OneFilter> RegistTest(){
        //通过FilterRegistrationBean实例设置优先级可以生效
        //通过@WebFilter无效
        FilterRegistrationBean<OneFilter> bean = new FilterRegistrationBean<OneFilter>();
        bean.setFilter(oneFilter);//注册自定义过滤器
        bean.setName("flilter1");//过滤器名称
        bean.addUrlPatterns("/*");//过滤所有路径
        bean.setOrder(1);//优先级，最顶级,越低越优先
        return bean;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //添加拦截器类
        registry.addInterceptor(oneHandler)
                //拦截要求
                .addPathPatterns("/**")
                //对哪些请求放行
                .excludePathPatterns("index.html","/favicon.ico","/","/login","/static/**");
    }
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //默认图片
        registry.addViewController("/favicon.ico").setViewName("/static/favicon.ico");
    }

    /**
     * 配置允许跨域
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/cors/**")
                .allowedHeaders("*")
                .allowedMethods("POST","GET")
                .allowedOrigins("*");
    }

    /**
     * 自定义404等错误页面
     * @return
     */
    @Bean
    public WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer() {
        return (factory -> {
            ErrorPage errorPage401 = new ErrorPage(HttpStatus.UNAUTHORIZED, "/static/401.html");
            ErrorPage errorPage404 = new ErrorPage(HttpStatus.NOT_FOUND, "/static/404.html");
            ErrorPage errorPage500 = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/static/500.html");
            factory.addErrorPages(errorPage401,errorPage404,errorPage500);
        });
    }
}
