package com.shuo.initial.config.handlers;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @auther 烁烁
 * @date 2020/5/11 12:50
 */
@Component
public class OneHandler implements HandlerInterceptor {
    static Logger logger = LoggerFactory.getLogger(OneHandler.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.info("OneHandler-preHandle");
        logger.info("请求的路径为: "+ request.getRequestURI() + ", 请求的参数为：" + JSON.toJSONString(request.getParameterMap()));
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        logger.info("OneHandler-postHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        logger.info("OneHandler-afterCompletion");
    }
}
