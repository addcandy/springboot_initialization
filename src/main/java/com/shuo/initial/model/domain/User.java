package com.shuo.initial.model.domain;

import java.io.Serializable;

/**
 * @auther 烁烁
 * @date 2020/4/27 11:21
 */

public class User implements Serializable {
    public static long serialVersionUID = 1L;
    private Integer id;
    private String  userName;
    private String  password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
