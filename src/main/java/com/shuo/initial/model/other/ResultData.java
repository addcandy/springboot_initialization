package com.shuo.initial.model.other;


import com.shuo.initial.model.enums.ResultEnum;

/**
 * @auther 烁烁
 * @date 2020/4/27 15:56
 */

public class ResultData<T> {
    private Integer code;
    private String msg;
    private T data;

    public static ResultData success(){
        return success(null);
    }
    public static <T> ResultData success(T data){
        return setting(ResultEnum.SUCCESS,data);
    }

    public static <T> ResultData setting(ResultEnum resultEnum,T data){
        return new ResultData(resultEnum.getCode(),resultEnum.getMsg(),data);
    }

    public ResultData(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
