package com.shuo.initial.model.enums;

public enum ResultEnum {
    SYSTEM_ERR(201,"系统错误"),
    SUCCESS(200,"成功"),
    LAYER_TABLE(0,"成功"),//layer的table使用
    ERROR(201,"失败");

    private Integer code;
    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
